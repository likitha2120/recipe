package com.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.dao.UserDAO;
import com.model.User;

import java.util.List;

@RestController
@CrossOrigin(origins="*")
public class UserController {

	    @Autowired
	    private UserDAO userDao;
	    
	    @GetMapping("getAllCustomers")
	    public List<User> getAllCustomers() {
	    	List<User> eList = userDao.getAllCustomers();
	    	System.out.println(eList);
	        return eList;
	    }
	    @PostMapping("register")
	    public void register(@RequestBody User
	    		customer) {
	        System.out.println("Data Received: "+customer);
	        userDao.register(customer);
	    }
	    
	    @GetMapping("getById/{id}")
	    public User getCustById(@PathVariable int id) {
	        return userDao.getCustomerById(id);
	    }
	    
	    @GetMapping("login/{email}/{password}")
	    public User validateCustomer(@PathVariable String email,@PathVariable String password){
	    	return userDao.getValidateCustomer(email,password);
	    }
	    
	    @DeleteMapping("/deleteCustomer/{id}")
	    public User deleteCustomerById(@PathVariable int id) {
	        return userDao.deleteCustomerById(id);
	    }
	    
	    @PutMapping("update")
	    public User updateCust(@RequestBody User employee){
			return userDao.updateCustomer(employee);
	    }
	}
