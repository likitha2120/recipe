package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Recipe {
	@Id
	@GeneratedValue
	private int id;
	private String imgUrl;
	private String recipeName ;
	private String recipeDescription;
	private String cookingTime;
	private String categoryType;
	public Recipe() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Recipe(int id, String imgUrl, String recipeName, String recipeDescription, String cookingTime,
			String categoryType) {
		super();
		this.id = id;
		this.imgUrl = imgUrl;
		this.recipeName = recipeName;
		this.recipeDescription = recipeDescription;
		this.cookingTime = cookingTime;
		this.categoryType = categoryType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public String getRecipeDescription() {
		return recipeDescription;
	}
	public void setRecipeDescription(String recipeDescription) {
		this.recipeDescription = recipeDescription;
	}
	public String getCookingTime() {
		return cookingTime;
	}
	public void setCookingTime(String cookingTime) {
		this.cookingTime = cookingTime;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	@Override
	public String toString() {
		return "Recipe [id=" + id + ", imgUrl=" + imgUrl + ", recipeName=" + recipeName + ", recipeDescription="
				+ recipeDescription + ", cookingTime=" + cookingTime + ", categoryType=" + categoryType + "]";
	}
	

}
